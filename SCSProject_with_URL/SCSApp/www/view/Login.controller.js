sap.ui.controller("scs.view.Login", {

/**
* Called when a controller is instantiated and its View controls (if available) are already created.
* Can be used to modify the View before it is displayed, to bind event handlers and do other one-time initialization.
* @memberOf scspro.login
*/
//	onInit: function() {
//
//	},
	
/**
* Similar to onAfterRendering, but this hook is invoked before the controller's View is re-rendered
* (NOT before the first rendering! onInit() is used for that one!).
* @memberOf scspro.login
*/
//	onBeforeRendering: function() {
//
//	},

/**
* Called when the View has been rendered (so its HTML is part of the document). Post-rendering manipulations of the HTML could be done here.
* This hook is the same one that SAPUI5 controls get after being rendered.
* @memberOf scspro.login
*/
//	onAfterRendering: function() {
//
//	},

/**
* Called when the Controller is destroyed. Use this one to free resources and finalize activities.
* @memberOf scspro.login
*/
//	onExit: function() {
//
//	}
	onLogin: function(){
		
		var username= this.getView().byId("username").getValue();
		var password= this.getView().byId("password").getValue();
		var ip = "";
		var path = "/Login";
		var data = JSON.stringify({
			  "USR_NAME": username,
			  "PASS": password,
			  "IP": ip
			});
		console.log(data);
			$.ajax({
				"async" : true,
				"crossDomain" : true,
				"url" : ServiceURL + path,
				"method" : "POST",
				"headers" : {
					"content-type" : "application/json",
					"cache-control" : "no-cache",
				},
				"processData" : false,
				"data" : data
			}).done(function(response) {
				console.log("response",response);
				
				sap.ui.getCore().byId("app1").to("idselectfacility");
				var fac = JSON.stringify({
					  "USR_NAME": "",
					});
				$.ajax({
					"async" : true,
					"crossDomain" : true,
					"url" : ServiceURL + "/Facilities",
					"method" : "POST",
					"headers" : {
						"content-type" : "application/json",
						"cache-control" : "no-cache",
					},
					"processData" : false,
					"data" : fac
				}).done(function(response) {
					console.log("response",response);
					var facjson = new sap.ui.model.json.JSONModel(response);
					oView.setModel(facjson);
				});
			});
		
	}

});