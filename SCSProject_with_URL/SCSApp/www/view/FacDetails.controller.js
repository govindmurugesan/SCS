var button="true";
sap.ui.controller("scs.view.FacDetails", {

/**
* Called when a controller is instantiated and its View controls (if available) are already created.
* Can be used to modify the View before it is displayed, to bind event handlers and do other one-time initialization.
* @memberOf scspro.s3
*/
	onInit: function() {
		/*this.getView().byId("idmenu").addStyleClass("hide");*/
		
		this.getView().byId("openMenu").attachBrowserEvent("tab keyup", function(oEvent){
			this._bKeyboard = oEvent.type == "keyup";
		}, this);
		
		/*this.getView().byId("fname").setValue(empdata.employeefname);*/
	},

/**
* Similar to onAfterRendering, but this hook is invoked before the controller's View is re-rendered
* (NOT before the first rendering! onInit() is used for that one!).
* @memberOf scspro.s3
*/
//	onBeforeRendering: function() {
//
//	},

/**
* Called when the View has been rendered (so its HTM	L is part of the document). Post-rendering manipulations of the HTML could be done here.
* This hook is the same one that SAPUI5 controls get after being rendered.
* @memberOf scspro.s3
*/
	onAfterRendering: function() {
		/*var json = sap.ui.getCore().getModel("facdatabase");
		sap.ui.getCore().byId("idfacdetails--facility_name").setText(json.oData.facname+" Summary");*/
		
	},

/**
* Called when the Controller is destroyed. Use this one to free resources and finalize activities.
* @memberOf scspro.s3
*/
//	onExit: function() {
//
//	}
	backtosel: function(){
		sap.ui.getCore().byId("app1").to("idselectfacility");
	},
	facility: function(){
		sap.ui.getCore().byId("app1").to("idselectfacility");
	},
	GoToRoom: function(id){
		/*var btn = this.getView().byId("room1").getText();*/
		/*var facid = $(".facid,.facid1,.facid2").attr("id");
		alert(facid)*/
	    var btn = this.getView().byId(id).getText();
	    console.log('btn',btn);
	    sap.ui.getCore().byId("idroomdetails--room_No").setValue(btn);
	    sap.ui.getCore().byId("roomid").setValue(btn);
	    var ssss = sap.ui.getCore().byId("idselectfacility--comboid").getSelectedItem().getText();
		console.log('ssss',ssss);
	    var idvalue = oView.getModel();
		for(k=1;k<idvalue.oData.FacilitiesResult.length;k++){
			if(ssss == idvalue.oData.FacilitiesResult[k].FACILITY_DISPLAY){
				var ID = idvalue.oData.FacilitiesResult[k].ID;
				alert(ID)
			}else{
				
			}
		}
	    var data = JSON.stringify({
			  "ID": ID,
			});
			$.ajax({
				"async" : true,
				"crossDomain" : true,
				"url" : ServiceURL + "/SelectRoomBins",
				"method" : "POST",
				"headers" : {
					"content-type" : "application/json",
					"cache-control" : "no-cache",
				},
				"processData" : false,
				"data" : data
			}).done(function(response) {
				console.log("response",response);
				var roomjson = new sap.ui.model.json.JSONModel(response);
				sap.ui.getCore().byId("app").setModel(roomjson,"roomjson");
				console.log(roomjson);
				sap.ui.getCore().byId("app1").to("idroomdetails");
			});
		
	},
	handlePressOpenMenu: function (oEvent) {
		var oButton = oEvent.getSource();
		var oDialog = this.getView().byId("handleMenuItemPress");
		var eDock = sap.ui.core.Popup.Dock;
		oDialog.open(this._bKeyboard, oButton, eDock.BeginTop, eDock.BeginBottom, oButton);
		
	},
		home: function(){
		sap.ui.getCore().byId("app1").to("idlogin");
	},

});

$(document).ready(function(){
	$(document).on("click",".facid",function(){
		callBack(this);
	});
});

function callBack(ele){
	var oController = sap.ui.getCore().byId("idfacdetails").getController();
	oController.GoToRoom($(ele).attr("id"));
} 